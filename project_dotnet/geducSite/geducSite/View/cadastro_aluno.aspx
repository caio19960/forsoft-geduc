﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="cadastro_aluno.aspx.cs" Inherits="geducSite.View.cadastro_aluno" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">
    
    <div class="box-content">
        <form id="form1" runat="server">
        
            <!-- Início do Código -->
            <fieldset>
    		<legend>Cadastro de Aluno
                <asp:Label ID="msg" runat="server"></asp:Label>
            </legend>
    			<div class="form-group">
                <h4> Login </h4>
                    <asp:Label ID="lblNomeUsuario" runat="server" Text="Usu&aacute;rio:" CssClass="col-sm-3 control-label"></asp:Label>
                    <asp:TextBox ID="txtUsuario" name="lblNomeUsuario" runat="server"></asp:TextBox><asp:Label ID="Label1" runat="server" Text="*" CssClass="form-control"/>                    
        								
        		    <asp:Label ID="lblSenha" runat="server" Text="Senha:" CssClass="col-sm-3 control-label"></asp:Label>
                    <asp:TextBox ID="txtSenha" name="lblSenha" runat="server"></asp:TextBox><asp:Label ID="Label2" runat="server" Text="*" CssClass="form-control"/>
                </div>
            </fieldset>			
    		
    			<h6>Dados Pessoais</h6>
                
                <asp:Label ID="lblNomeAluno" runat="server" Text="Nome:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:TextBox ID="txtNomeAluno" name="lblNomeAluno" runat="server" CssClass="x12"></asp:TextBox>
            <asp:Label ID="Label3" runat="server" Text="*" CssClass="form-control"/>
    			

                <asp:Label ID="lblDataNascimento" runat="server" Text="Data de Nascimento:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:TextBox ID="txtDataNascimento" name="lblDataNascimento" runat="server" CssClass="x4" TextMode="date"></asp:TextBox><asp:Label ID="Label4" runat="server" Text="*" CssClass="form-control"/>
    			
    							
    			<asp:Label ID="lblSexo" runat="server" Text="Sexo:" CssClass="col-sm-3 control-label"></asp:Label>	
                <asp:RadioButtonList ID="rbSexo" runat="server">
                    <asp:ListItem Value="Masculino" Text="Masculino" Selected="True"/>
                    <asp:ListItem Value="Feminino" Text="Feminino" /> 
                </asp:RadioButtonList>																													
                <asp:Label ID="Label5" runat="server" Text="*" CssClass="form-control"/> 
                

    			<asp:Label ID="lblNaturalidade" runat="server" Text="Naturalidade:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:TextBox ID="txtNaturalidade" name="lblNaturalidade" runat="server" CssClass="x4"></asp:TextBox><asp:Label ID="Label6" runat="server" Text="*" CssClass="form-control"/>
    			

                <asp:Label ID="lblNacionalidade" runat="server" Text="Nacionalidade:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:TextBox ID="txtNacionalidade" name="lblNacionalidade" runat="server" CssClass="x4"></asp:TextBox><asp:Label ID="Label7" runat="server" Text="*" CssClass="form-control"/>
    			

                <asp:Label ID="lblNomePai" runat="server" Text="Nome do Pai:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:TextBox ID="txtNomePai" name="lblNomePai" runat="server" CssClass="x4"></asp:TextBox><asp:Label ID="Label8" runat="server" Text="*" CssClass="form-control"/>
    			

                <asp:Label ID="lblNomeMae" runat="server" Text="Nome da M&atilde;e:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:TextBox ID="txtNomeMae" name="lblNomeMae" runat="server" CssClass="x4"></asp:TextBox><asp:Label ID="Label9" runat="server" Text="*" CssClass="form-control"/>
    			
    															
    			<asp:Label ID="lblEtnia" runat="server" Text="Etnia:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:DropDownList ID="ddlEtnia" name="lblEtnia" runat="server">
                    <asp:ListItem>Branco(a)</asp:ListItem>
                    <asp:ListItem>Negro(a)</asp:ListItem>
                    <asp:ListItem>Amarelo(a)</asp:ListItem>
                    <asp:ListItem>Pardo</asp:ListItem>
                    <asp:ListItem>Ind&iacute;gena</asp:ListItem>
                    <asp:ListItem>Sem Declara&ccedil;&atilde;o</asp:ListItem>
                </asp:DropDownList><asp:Label ID="Label10" runat="server" Text="*" CssClass="form-control"/>
                

                <asp:Label ID="lblEstadoCivil" runat="server" Text="Estado Civil:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:DropDownList ID="ddlEstadoCivil" name="lblEstadoCivil" runat="server">
                    <asp:ListItem>Solteiro (a)</asp:ListItem>
                    <asp:ListItem>Casado (a)</asp:ListItem>
                    <asp:ListItem>Divorciado (a)</asp:ListItem>
                    <asp:ListItem>Vi&uacute;vo (a)</asp:ListItem>
                    <asp:ListItem>Sem Declara&ccedil;&atilde;o</asp:ListItem>
                </asp:DropDownList><asp:Label ID="Label11" runat="server" Text="*" CssClass="form-control"/>
                

                <asp:Label ID="lblEscolaridade" runat="server" Text="N&iacute;vel de Escolaridade:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:DropDownList ID="ddlEscolaridade" name="lblEscolaridade" runat="server">
                    <asp:ListItem>Escolha uma op&ccedil;&atilde;o</asp:ListItem>
                    <asp:ListItem>Superior</asp:ListItem>
                    <asp:ListItem>Superior Incompleto</asp:ListItem>
                    <asp:ListItem>T&eacute;cnico</asp:ListItem>
                    <asp:ListItem>T&eacute;cnico Incompleto</asp:ListItem>
                    <asp:ListItem>M&eacute;dio</asp:ListItem>
                    <asp:ListItem>M&eacute;dio Incompleto</asp:ListItem>
                    <asp:ListItem>Fundamental</asp:ListItem>
                    <asp:ListItem>Fundamental Incompleto</asp:ListItem>
                </asp:DropDownList><asp:Label ID="Label12" runat="server" Text="*" CssClass="form-control"/>
                

                <asp:Label ID="lblNecessidadeEspecial"  runat="server" Text="Possui alguma necessidade especial?" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:RadioButtonList ID="rbNecessidadeEspecial"	runat="server">
                    <asp:ListItem Value="Sim" Text="Sim" />
                    <asp:ListItem Value="Não" Text="Não" />
                </asp:RadioButtonList>
                <asp:Label ID="Label13" runat="server" Text="*" CssClass="form-control"/> 
                
    								
    								
    			<h6>Aluno</h6>

                <asp:Label ID="lblMatricula" runat="server" Text="Matr&iacute;cula:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:TextBox ID="txtMatricula" name="lblMatricula" runat="server" CssClass="x4"></asp:TextBox><asp:Label ID="Label14" runat="server" Text="*" CssClass="form-control"/>
    			
    								
                <asp:Label ID="lblSituacao" runat="server" Text="Situa&ccedil;&atilde;o:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:DropDownList ID="ddlSituacao" name="lblSituacao" runat="server">
                    <asp:ListItem>Escolha uma op&ccedil;&atilde;o</asp:ListItem>
                    <asp:ListItem Selected="True" >Ativo</asp:ListItem>
                    <asp:ListItem>Inativo</asp:ListItem>                      
                </asp:DropDownList><asp:Label ID="Label15" runat="server" Text="*" CssClass="form-control"/>
                
    								
    								
    			<h6> Documenta&ccedil;ao </h6>

    			<asp:Label ID="lblCPF" runat="server" Text="CPF:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:TextBox ID="txtCPF" name="lblCPF" runat="server" CssClass="x4"></asp:TextBox><asp:Label ID="Label16" runat="server" Text="*" CssClass="form-control"/>								

                <asp:Button ID="btnVerificar" runat="server" Text="verificar" />
                
                                    
                <asp:Label ID="lblRG" runat="server" Text="RG:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:TextBox ID="txtRG" name="lblRG" runat="server" CssClass="x4"></asp:TextBox><asp:Label ID="Label17" runat="server" Text="*" CssClass="form-control"/>   
    			

    			<asp:Label ID="lblExpedido" runat="server" Text="Data de Expedi&ccedil;&atilde;o:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:TextBox ID="txtDataExpedicao"  name="lblExpedido" runat="server" CssClass="x4" TextMode="date"></asp:TextBox><asp:Label ID="Label18" runat="server" Text="*" CssClass="form-control"/>   
    			

    			<asp:Label ID="lblOrgao" runat="server" Text="Org&atilde;o Expedidor:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:TextBox ID="txtOrgao" name="lblOrgao" runat="server" CssClass="x4"></asp:TextBox><asp:Label ID="Label19" runat="server" Text="*" CssClass="form-control"/>   
    			

    			<h6>Certid&atilde;o de Nascimento:</h6>
    								
                <asp:Label ID="lblNumCertidaoNascimento" runat="server" Text="N&uacute;mero:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:TextBox ID="txtNumCertidaoNascimento" name="lblNumCertidaoNascimento" runat="server" CssClass="x4"></asp:TextBox><asp:Label ID="Label20" runat="server" Text="*" CssClass="form-control"/>   
    			

    			<asp:Label ID="lblLivroCertidaoNascimento" runat="server" Text="Livro:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:TextBox ID="txtLivroCertidaoNascimento" name="lblLivroCertidaoNascimento" runat="server" CssClass="x4"></asp:TextBox><asp:Label ID="Label21" runat="server" Text="*" CssClass="form-control"/>   
    			

    			<asp:Label ID="lblFolhaCertidaoNascimento" runat="server" Text="Folha:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:TextBox ID="txtFolhaCertidaoNascimento" name="lblFolhaCertidaoNascimento" runat="server" CssClass="x4"></asp:TextBox><asp:Label ID="Label22" runat="server" Text="*" CssClass="form-control"/>   
    			

    			<asp:Label ID="lblDataCertidaoNascimento" runat="server" Text="Data de Emiss&atilde;o:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:TextBox ID="txtDataCertidaoNascimento" name="lblDataCertidaoNascimento" runat="server" CssClass="x4" TextMode="date"></asp:TextBox><asp:Label ID="Label23" runat="server" Text="*" CssClass="form-control"/>   
    			
            								
                <asp:Label ID="lblTituloEleitor" runat="server" Text="T&iacute;tulo de Eleitor:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:TextBox ID="txtTituloEleitor" name="lblTituloEleitor" runat="server" CssClass="x4"></asp:TextBox><asp:Label ID="Label24" runat="server" Text="*" CssClass="form-control"/>   
    			

                <asp:Label ID="lblCertificadoReservista" runat="server" Text="Certificado de Reservista:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:TextBox ID="txtCertificadoReservista" name="lblCertificadoReservista" runat="server" CssClass="x4"></asp:TextBox><asp:Label ID="Label25" runat="server" Text="*" CssClass="form-control"/>   
    			

    								
    			<h6> Endere&ccedil;o </h6>

    			<asp:Label ID="lblLogradouro" runat="server" Text="Logradouro:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:TextBox ID="txtLogradouro" name="lblLogradouro" runat="server" CssClass="x4"></asp:TextBox><asp:Label ID="Label26" runat="server" Text="*" CssClass="form-control"/>   
    			

                <asp:Label ID="lblNumero" runat="server" Text="N&uacute;mero:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:TextBox ID="txtNumero" name="lblNumero" runat="server" CssClass="x4"></asp:TextBox><asp:Label ID="Label27" runat="server" Text="*" CssClass="form-control"/>   
    			
    								
    			<asp:Label ID="lblComplemento" runat="server" Text="Complemento:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:TextBox ID="txtComplemento" name="lblComplemento" runat="server" CssClass="x4"></asp:TextBox><asp:Label ID="Label28" runat="server" Text="*" CssClass="form-control"/>   
    			

    			<asp:Label ID="lblBairro" runat="server" Text="Bairro:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:TextBox ID="txtBairro" name="lblBairro" runat="server" CssClass="x4"></asp:TextBox><asp:Label ID="Label29" runat="server" Text="*" CssClass="form-control"/>   
    			

    			<asp:Label ID="lblCidade" runat="server" Text="Cidade:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:TextBox ID="txtCidade" name="lblCidade" runat="server" CssClass="x4"></asp:TextBox><asp:Label ID="Label30" runat="server" Text="*" CssClass="form-control"/>   
    			

                <asp:Label ID="lblUF" runat="server" Text="UF:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:DropDownList ID="ddlUF" name="lblUF" runat="server">
                    <asp:ListItem></asp:ListItem>
    				<asp:ListItem>AC</asp:ListItem>
                    <asp:ListItem>AL</asp:ListItem> 
                    <asp:ListItem>AP</asp:ListItem> 
                    <asp:ListItem>AM</asp:ListItem> 
                    <asp:ListItem>BA</asp:ListItem> 
                    <asp:ListItem>CE</asp:ListItem> 
                    <asp:ListItem>DF</asp:ListItem> 
                    <asp:ListItem>ES</asp:ListItem> 
                    <asp:ListItem>GO</asp:ListItem> 
                    <asp:ListItem>MA</asp:ListItem> 
                    <asp:ListItem>MT</asp:ListItem> 
                    <asp:ListItem>MS</asp:ListItem> 
                    <asp:ListItem>MG</asp:ListItem> 
                    <asp:ListItem>PA</asp:ListItem> 
                    <asp:ListItem>PB</asp:ListItem> 
                    <asp:ListItem>PR</asp:ListItem> 
                    <asp:ListItem>PE</asp:ListItem> 
                    <asp:ListItem>PI</asp:ListItem> 
                    <asp:ListItem Selected="True">RJ</asp:ListItem> 
                    <asp:ListItem >RN</asp:ListItem> 
                    <asp:ListItem>RS</asp:ListItem> 
                    <asp:ListItem>RO</asp:ListItem> 
                    <asp:ListItem>RR</asp:ListItem> 
                    <asp:ListItem>SC</asp:ListItem> 
                    <asp:ListItem>SP</asp:ListItem> 
                    <asp:ListItem>SE</asp:ListItem> 
                    <asp:ListItem>TO</asp:ListItem>  	                     
                </asp:DropDownList><asp:Label ID="Label31" runat="server" Text="*" CssClass="form-control"/>
                
    											
    			<asp:Label ID="lblCEP" runat="server" Text="CEP:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:TextBox ID="txtCEP" name="lblCEP" runat="server" CssClass="x4"></asp:TextBox><asp:Label ID="Label32" runat="server" Text="*" CssClass="form-control"/>   
    			

                <asp:Label ID="lblMunicipio" runat="server" Text="Munic&iacute;pio:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:TextBox ID="txtMunicipio" name="lblMunicipio" runat="server" CssClass="x4"></asp:TextBox><asp:Label ID="Label33" runat="server" Text="*" CssClass="form-control"/>   
    			


    			<asp:Label ID="lblZona" runat="server" Text="Zona:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:TextBox ID="txtZona" name="lblZona" runat="server" CssClass="x4"></asp:TextBox><asp:Label ID="Label34" runat="server" Text="*" CssClass="form-control"/>   
    			
    			
    								
    			<h6>Contato </h6>
    								
    			<asp:Label ID="lblTelefone" runat="server" Text="Telefone:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:TextBox ID="txtTelefone" name="lblTelefone" runat="server" CssClass="x4"></asp:TextBox><asp:Label ID="Label35" runat="server" Text="*" CssClass="form-control"/>   
    			

    			<asp:Label ID="lblCelular" runat="server" Text="Celular:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:TextBox ID="txtCelular" name="lblCelular" runat="server" CssClass="x4"></asp:TextBox><asp:Label ID="Label36" runat="server" Text="*" CssClass="form-control"/>   
    			

    			<asp:Label ID="lblEmail" runat="server" Text="E-mail:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:TextBox ID="txtEmail" name="lblEmail" runat="server" CssClass="x4"></asp:TextBox><asp:Label ID="Label37" runat="server" Text="*" CssClass="form-control"/>   
    			
    																							
    			<asp:Label ID="lblOutros" runat="server" Text="Outros:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:TextBox ID="txtOutros" name="lblOutros" runat="server" CssClass="x4"></asp:TextBox><asp:Label ID="Label38" runat="server" Text="*" CssClass="form-control"/>   
    			

                <h6>Curso:</h6>

                <asp:Label ID="lblCurso" runat="server" Text="Selecione o Curso do aluno:" />
                <asp:DropDownList ID="ddlCurso" runat="server">
                </asp:DropDownList>
                
            								
    			<h6> Programa Social </h6>

                <asp:Label ID="lblProgramaSocial"  runat="server" Text="Participa de um programa social?" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:RadioButtonList ID="rbProgamaSocial" runat="server">
                    <asp:ListItem Value="Sim" Text="Sim" />
                    <asp:ListItem Value="não" Text="Não" />
                </asp:RadioButtonList>
                <asp:Label ID="Label39" runat="server" Text="*" CssClass="form-control"/>  
                

    			<!--"caso o usuario aperte em "sim" devera abrir os seguintes campos: (java script/ jquery" -->	
                <asp:Label ID="nomeProgramaSocial" runat="server" Text="Nome:" CssClass="col-sm-3 control-label"></asp:Label>
                <asp:DropDownList ID="ddlProgramaSocial" name="nomeProgramaSocial" runat="server">          
                </asp:DropDownList><asp:Label ID="Label40" runat="server" Text="*" CssClass="form-control"/>
                
    				<!--<span id="msg"> Mensagem </span> -->
    				
                    <asp:Button ID="btnVoltar" runat="server" Text="Voltar" />
                    <asp:Button ID="btnLimpar" Text="Limpar" runat="server" OnClick="btnLimpar_Click"/>
                    <asp:Button ID="btnCadastrar" Text="Cadastrar" runat="server" OnClick="btnCadastrar_Click"/>				                    
    				
             <!-- Fim do Código -->
        </form>
    </div>
</asp:Content>
