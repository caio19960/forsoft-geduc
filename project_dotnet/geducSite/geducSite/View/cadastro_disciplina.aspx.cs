﻿using geducSite.Models;
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace geducSite.View
{
    public partial class cadastro_disciplina : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnCadastrar_Click(object sender, EventArgs e)
        {
            if (txtNomeDisciplina.Text != "" || txtCodigoDisciplina.Text != "" || txtCargaHorariaDisciplina.Text != "")
            {

                Disciplina dis = new Disciplina(); 
                dis.nome = txtNomeDisciplina.Text;
                dis.codigo = txtCodigoDisciplina.Text;
                dis.cargaHoraria = Convert.ToInt32(txtCargaHorariaDisciplina.Text);

                new DisciplinaDAO().Cadastrar(dis);

                msg.InnerText = "Cadastrado com sucesso!!!";
            }
            else
            {

                msg.InnerText = "Campo não preencido";
            }
            limpar();
        }

        public void limpar()
        {
            txtNomeDisciplina.Text = string.Empty;
            txtCodigoDisciplina.Text = string.Empty;
            txtCargaHorariaDisciplina.Text = string.Empty;
        }
    }
}