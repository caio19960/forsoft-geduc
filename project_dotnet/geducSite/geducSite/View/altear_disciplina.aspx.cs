﻿using geducSite.Models;
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace geducSite.View
{
    public partial class altear_disciplina : System.Web.UI.Page
    {

        protected IEnumerable<Disciplina> todasAsDisciplina;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ListaFuncionarios"] != null)
            {
                todasAsDisciplina = (IEnumerable<Disciplina>)Session["todasAsDisciplina"];
            }
            else
            {
                todasAsDisciplina = new DisciplinaDAO().ListarDisciplinas();
                Session["todasAsDisciplina"] = todasAsDisciplina;
                Session.Timeout = 6000;
            }
        }

        protected void btnAlterar_Click(object sender, EventArgs e)
        {
            if (txtNomeDisciplina.Text != "" || txtCodigoDisciplina.Text != "" || txtCargaHorariaDisciplina.Text != "")
            {

                Disciplina dis = todasAsDisciplina.SingleOrDefault(x => x.codigo == txtCodigoDisciplina.Text);
                if (dis != null)
                {
                    dis.nome = txtNomeDisciplina.Text;
                    dis.codigo = txtCodigoDisciplina.Text;
                    dis.cargaHoraria = Convert.ToInt32(txtCargaHorariaDisciplina.Text);

                    new DisciplinaDAO().Atualizar(dis);

                    msg.InnerText = "Alterado com sucesso!!!";
                }
            }
            else
            {

                msg.InnerText = "Campo não preencido";
            }
            limpar();
        }

        public void limpar()
        {
            txtNomeDisciplina.Text = string.Empty;
            txtCodigoDisciplina.Text = string.Empty;
            txtCargaHorariaDisciplina.Text = string.Empty;
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            Disciplina d = todasAsDisciplina.SingleOrDefault(x => x.codigo == txtCodigoDisciplina.Text);
            if (d != null)
            {
                txtNomeDisciplina.Text = d.nome;
                txtCargaHorariaDisciplina.Text = Convert.ToString(d.cargaHoraria);
            }
        }
    }
}