﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/layout.Master" AutoEventWireup="true" CodeBehind="cadastro_funcionario.aspx.cs" Inherits="geducSite.View.cadastro_funcionario" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Conteudo" runat="server">
    <div class="row">
            <div class="box-content">
                <form id="Formulario" runat="server">
                    <fieldset>
                        <legend class="text-center">Funcion&aacute;rio</legend>
                        <fieldset>
                            <legend class="text-center">Dados Pessoais</legend>
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblUsuario" runat="server" Text="Usuario:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtUsuario" MaxLength="50" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblSenha" runat="server" Text="Senha:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtSenha" MaxLength="50" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblPgerfilAcesso" runat="server" Text="Perfil de Acesso:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:DropDownList ID="ddlPerfilAcesso" CssClass="width-50" runat="server">
                                        <asp:ListItem>Perfil</asp:ListItem>
                                        <asp:ListItem Value="A">Aluno/Responsavel </asp:ListItem>
                                        <asp:ListItem Value="B">Professor</asp:ListItem>
                                        <asp:ListItem Value="C">Secretaria da instituição </asp:ListItem>
                                        <asp:ListItem Value="D">Diretor</asp:ListItem>
                                        <asp:ListItem Value="E">Ministério da Educação</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <br />
                        </fieldset>
                        <fieldset>
                            <legend class="text-center">Dados Pessoais </legend>
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblNomeFuncionario" runat="server" Text="Nome:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtNomeFuncionario" MaxLength="50" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblMatricula" runat="server" Text="Matricula:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtMatricula" MaxLength="50" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblDataNascimento" runat="server" Text="Data de Nascimento:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtDataNascimento" TextMode="Date" MaxLength="50" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblSexo" runat="server" Text="Sexo:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:RadioButton ID="rbSexoM" runat="server" GroupName="sexo" Text="Masculino" />
                                    <asp:RadioButton ID="rbSexoF" runat="server" GroupName="sexo" Text="Feminino" />
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblNaturalidade" runat="server" Text="Naturalidade:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtNaturalidade" MaxLength="50" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblNacionalidade" runat="server" Text="Nacionalidade:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtNacionalidade" MaxLength="50" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblNomePai" runat="server" Text="Nome do Pai:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtNomePai" MaxLength="50" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblNomeMae" runat="server" Text="Nome da M&atilde;e:"></asp:Label>

                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtNomeMae" MaxLength="50" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblEtnia" runat="server" Text="Etnia:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:DropDownList ID="ddlEtnia" runat="server" CssClass="width-50">
                                        <asp:ListItem Text="Selecione uma Op&ccedil;&atilde;o" Value="selecione" />
                                        <asp:ListItem Text="Branco" Value="branco" />
                                        <asp:ListItem Text="Preta" Value="preta" />
                                        <asp:ListItem Text="Amarela" Value="amarela" />
                                        <asp:ListItem Text="Pardo" Value="pardo" />
                                        <asp:ListItem Text="Ind&iacute;gena" Value="pardo" />
                                        <asp:ListItem Text="Sem Declara&ccedil;&atilde;o" Value="sem" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblEstadoCivil" runat="server" Text="Estado Civil:"></asp:Label>
                                <div class="col-sm-10">

                                    <asp:DropDownList ID="ddlEstadoCivil" runat="server" CssClass="width-50">
                                        <asp:ListItem Text="Selecione uma Op&ccedil;&atilde;o" Value="selecione" />
                                        <asp:ListItem Text="Solteiro(a)" Value="solteiro" />
                                        <asp:ListItem Text="Casado(a)" Value="casado" />
                                        <asp:ListItem Text="Divorciado(a)" Value="divorciado" />
                                        <asp:ListItem Text="Vi&uacute;vo (a)" Value="viuvo" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblEscolaridade" runat="server" Text="N&iacute;vel de Escolaridade:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:DropDownList ID="ddlEscolaridade" runat="server" CssClass="width-50">
                                        <asp:ListItem Text="Selecione uma Op&ccedil;&atilde;o" Value="selecione" />
                                        <asp:ListItem Text="Superior" Value="superior" />
                                        <asp:ListItem Text="Superior Incompleto" Value="superioin" />
                                        <asp:ListItem Text="T&eacute;cnico" Value="tecnico" />
                                        <asp:ListItem Text="T&eacute;cnico Incompleto (a)" Value="tecnicoin" />
                                        <asp:ListItem Text="M&eacute;dio" Value="medio" />
                                        <asp:ListItem Text="Vi&M&eacute;dio Incompleto" Value="medioin" />
                                        <asp:ListItem Text="Fundamental" Value="fundamental" />
                                        <asp:ListItem Text="Fundamental Incompleto" Value="fundamentalin" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblNecessidadeEspecial" runat="server" Text="Possui alguma necessidade especial?:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:RadioButton ID="rbNecessidadeSim" Checked="true" runat="server" GroupName="necessidade" Text="Sim" />
                                    <asp:RadioButton ID="rbNecessidadeNao" Checked="false" runat="server" GroupName="necessidade" Text="Não" />
                                </div>
                            </div>
                            <br />

                        </fieldset>
                        <fieldset>
                            <legend class="text-center">Funcion&aacute;rio</legend>
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="Cargo" runat="server" Text="Cargo:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:DropDownList ID="ddlCargos" CssClass="width-50" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblSituacao" runat="server" Text="Situa&ccedil;&atilde;o:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:DropDownList ID="ddlSituacao" runat="server" CssClass="width-50">
                                        <asp:ListItem Text="Selecione uma Op&ccedil;&atilde;o" Value="selecione" />
                                        <asp:ListItem Text="Ativo" Value="ativo" />
                                        <asp:ListItem Text="Desativo" Value="inativo" />

                                    </asp:DropDownList>
                                </div>
                            </div>
                            <br />
                            <h4>Documenta&ccedil;ao </h4>
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblCpf" runat="server" Text="CPF:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtCpf" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblRg" runat="server" Text="RG:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtrg" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblExpedido" runat="server" Text="Data de Expedi&ccedil;&atilde;o:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtExpedido" TextMode="Date" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblOrgao" runat="server" Text="Org&atilde;o Expedidor:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtOrgao" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <p>Certid&atilde;o de Nascimento:</p>
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblNumCertidaoNascimento" runat="server" Text="N&uacute;mero:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtNumCertificadoNascimento" TextMode="Number" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblLivroCertidaoNascimento" runat="server" Text="Livro"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtLivroCertidaoNascimento" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblFolhaCertidaoNascimento" runat="server" Text="Folha:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtFolhaCertidaoNascimento" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblDataCertidaoNascimento" runat="server" Text="Data de Emiss&atilde;o:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtDataCertidaoNascimento" TextMode="Date" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblTituloEleitor" runat="server" Text="T&iacute;tulo de Eleitor:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtTituloEleito" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblCertificadoReservista" runat="server" Text="Certificado de Reservista:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtCertificadoReservista" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                        </fieldset>

                        <fieldset>
                            <legend class="text-center">Endere&ccedil;o </legend>
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblLogradouro" runat="server" Text="Logradouro:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtLogradouro" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblNumero" runat="server" Text="N&uacute;mero:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtNumero" TextMode="Number" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblComplemento" runat="server" Text="Complemento:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtComplemento" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblBairro" runat="server" Text="Bairro:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtBairro" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblCidade" runat="server" Text="Cidade:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtCidade" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblCEP" runat="server" Text="CEP:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtCEP" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lbl" runat="server" Text="UF:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:DropDownList ID="ddlUF" CssClass="width-50" runat="server">
                                        <asp:ListItem Value=" ">  </asp:ListItem>
                                        <asp:ListItem Value="AC">AC</asp:ListItem>
                                        <asp:ListItem Value="AL">AL</asp:ListItem>
                                        <asp:ListItem Value="AP">AP</asp:ListItem>
                                        <asp:ListItem Value="AM">AM</asp:ListItem>
                                        <asp:ListItem Value="BA">BA</asp:ListItem>
                                        <asp:ListItem Value="CE">CE</asp:ListItem>
                                        <asp:ListItem Value="DF">DF</asp:ListItem>
                                        <asp:ListItem Value="ES">ES</asp:ListItem>
                                        <asp:ListItem Value="GO">GO</asp:ListItem>
                                        <asp:ListItem Value="MA">MA</asp:ListItem>
                                        <asp:ListItem Value="MT">MT</asp:ListItem>
                                        <asp:ListItem Value="MS">MS</asp:ListItem>
                                        <asp:ListItem Value="MG">MG</asp:ListItem>
                                        <asp:ListItem Value="PA">PA</asp:ListItem>
                                        <asp:ListItem Value="PB">PB</asp:ListItem>
                                        <asp:ListItem Value="PR">PR</asp:ListItem>
                                        <asp:ListItem Value="PE">PE</asp:ListItem>
                                        <asp:ListItem Value="PI">PI</asp:ListItem>
                                        <asp:ListItem Value="RJ">RJ</asp:ListItem>
                                        <asp:ListItem Value="RN">RN</asp:ListItem>
                                        <asp:ListItem Value="RS">RS</asp:ListItem>
                                        <asp:ListItem Value="RO">RO</asp:ListItem>
                                        <asp:ListItem Value="RR">RR</asp:ListItem>
                                        <asp:ListItem Value="SC">SC</asp:ListItem>
                                        <asp:ListItem Value="SP">SP</asp:ListItem>
                                        <asp:ListItem Value="SE">SE</asp:ListItem>
                                        <asp:ListItem Value="TO">TO</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblMunicipio" runat="server" Text="Munic&iacute;pio:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtMunicipio" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblZona" runat="server" Text="Zona:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtZona" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                        </fieldset>
                        <fieldset>
                            <legend class="text-center">Contato </legend>
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblTelefone" runat="server" Text="Telefone:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtTelefone" TextMode="Phone" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblCelular" runat="server" Text="Celular:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtCelular" TextMode="Phone" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblEmail" runat="server" Text="E-mail:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtEmail" TextMode="Email" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblOutros" runat="server" Text="Outros:"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="txtOutros" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                        </fieldset>
                        <fieldset>
                            <legend class="text-center">Programa Social </legend>
                            <div class="form-group">
                                <asp:Label CssClass="col-sm-2 control-label" ID="lblProgramaSocial" runat="server" Text="Participa de algum programa social"></asp:Label>
                                <div class="col-sm-10">
                                    <asp:RadioButton ID="rbProgramaSocialN" runat="server" GroupName="ProgramaSocial" Text="N&atilde;o" />
                                    <asp:RadioButton ID="rbProgramaSocialS" runat="server" GroupName="ProgramaSocial" Text="Sim" />
                                </div>
                            </div>
                            <br />
                            <div class="form-group">
                                <div class="col-sm-10">
                                    <asp:DropDownList ID="ddlProgramaSocial" CssClass="width-50" runat="server" />
                                </div>
                            </div>
                            <br />
                        </fieldset>
                        <br />
                        <asp:Button ID="btnLimpar" runat="server" Text="Limpar" OnClick="btnLimpar_Click" />
                        <asp:Button ID="brnListar" runat="server" Text="listar" OnClick="brnListar_Click" />
                        <asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClick="btnBuscar_Click" />
                        <asp:Button ID="btnAlterar" runat="server" Text="Alterar" OnClick="btnAlterar_Click" />
                        <asp:Button ID="Cadastrar" runat="server" Text="Cadastrar" OnClick="Cadastrar_Click" />

                    </fieldset>
                </form>
            </div>
    </div>
</asp:Content>
