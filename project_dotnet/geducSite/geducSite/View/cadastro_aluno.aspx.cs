﻿using geducSite.Models;
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace geducSite.View
{
    public partial class cadastro_aluno : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                try
                {
                    ProgramaSocialDAO psd = new ProgramaSocialDAO();
                    DataTable dtp = psd.preencherDropPrograma();
                    ddlProgramaSocial.DataSource = dtp;
                    ddlProgramaSocial.DataValueField = "idProgramaSocial";
                    ddlProgramaSocial.DataTextField = "nomePrograma";
                    ddlProgramaSocial.DataBind();

                    CursoDAO cd = new CursoDAO();
                    DataTable dtc = cd.preencherDropCurso();
                    ddlCurso.DataSource = dtc;
                    ddlCurso.DataValueField = "idCurso";
                    ddlCurso.DataTextField = "nomeC";
                    ddlCurso.DataBind();

                }
                catch(Exception ex)
                {
                    msg.Text = ex.Message;
                }
                
            }

        }

        protected void btnCadastrar_Click(object sender, EventArgs e)
        {
            try
            {
                Aluno a = new Aluno();
                a.contato = new Contato();
                a.curso = new Curso();
                a.endereco = new Endereco();
                a.login = new Models.Login();
                a.programaSocial = new ProgramaSocial();
                a.documento = new Documento();

                AlunoDAO ad = new AlunoDAO();

                //Login
                a.login.usuario = txtUsuario.Text;
                a.login.senha = txtSenha.Text;
                a.login.perfilAcesso = "Aluno";

                //Dados Pessoais
                a.nome = txtNomeAluno.Text;
                a.dataNascimento = Convert.ToDateTime(txtDataNascimento.Text);
                a.sexo = rbSexo.SelectedValue;
                a.naturalidade = txtNaturalidade.Text;
                a.nacionalidade = txtNacionalidade.Text;
                a.nomePai = txtNomePai.Text;
                a.nomeMae = txtNomeMae.Text;
                a.etnia = ddlEtnia.SelectedValue;
                a.estadoCivil = ddlEstadoCivil.SelectedValue;
                a.nivelEscolaridade = ddlEscolaridade.SelectedValue;
                a.necessidadeEsp = rbNecessidadeEspecial.SelectedValue;

                //Dados de Aluno
                a.situacao = ddlSituacao.SelectedValue;
                a.matricula = txtMatricula.Text;

                //Documentação
                a.documento.cpf = txtCPF.Text;
                a.documento.rg = txtRG.Text;
                a.documento.dataExpedicao = Convert.ToDateTime(txtDataExpedicao.Text);
                a.documento.orgaoExpedidor = txtOrgao.Text;
                a.documento.numCertidao = txtNumCertidaoNascimento.Text;
                a.documento.livroCertidao = txtLivroCertidaoNascimento.Text;
                a.documento.folhaCertidao = txtFolhaCertidaoNascimento.Text;
                a.documento.dataEmiCertidao = Convert.ToDateTime(txtDataCertidaoNascimento.Text);
                a.documento.titEleitor = txtTituloEleitor.Text;
                a.documento.certReservista = txtCertificadoReservista.Text;

                //Endereco
                a.endereco.longradouro = txtLogradouro.Text;
                a.endereco.numero = txtNumero.Text;
                a.endereco.complemento = txtComplemento.Text;
                a.endereco.bairro = txtBairro.Text;
                a.endereco.cidade = txtCidade.Text;
                a.endereco.uf = ddlUF.SelectedValue;
                a.endereco.cep = txtCEP.Text;
                a.endereco.municipio = txtMunicipio.Text;
                a.endereco.zona = txtZona.Text;

                //Contato
                a.contato.telefoneFixo = txtTelefone.Text;
                a.contato.telefoneCelular = txtCelular.Text;
                a.contato.outros = txtOutros.Text;
                a.contato.email = txtEmail.Text;

                //Curso
                a.curso.idCurso = Convert.ToInt32(ddlCurso.SelectedValue);
                
                //Programa Social
                a.programaSocial.idProgramaSocial = Convert.ToInt32(ddlProgramaSocial.SelectedValue);
                
                //Salvando o Aluno
                ad.salvar(a);

                limparCampos();

                msg.Text = "O aluno " + a.nome + ", foi cadastrado com sucesso!";
            }
            catch (Exception ex)
            {
                msg.Text = ex.Message;
            }
        }

        private void limparCampos()
        {
            //Login
            txtUsuario.Text = null;
            txtSenha.Text = null;
            
            //Dados Pessoais
            txtNomeAluno.Text = null;
            txtNaturalidade.Text = null;
            txtNacionalidade.Text = null;
            txtNomePai.Text = null;
            txtNomeMae.Text = null;
            ddlEtnia.SelectedIndex = 0;
            ddlEstadoCivil.SelectedIndex = 0;
            ddlEscolaridade.SelectedIndex = 0;

            //Dados de Aluno
            ddlSituacao.SelectedIndex = 0;
            txtMatricula.Text = null;

            //Documentação
            txtCPF.Text = null;
            txtRG.Text = null;
            txtOrgao.Text = null;
            txtNumCertidaoNascimento.Text = null;
            txtLivroCertidaoNascimento.Text = null;
            txtFolhaCertidaoNascimento.Text = null;
            txtTituloEleitor.Text = null;
            txtCertificadoReservista.Text = null;

            //Endereco
            txtLogradouro.Text = null;
            txtNumero.Text = null;
            txtComplemento.Text = null;
            txtBairro.Text = null;
            txtCidade.Text = null;
            ddlUF.SelectedIndex = 0;
            txtCEP.Text = null;
            txtMunicipio.Text = null;
            txtZona.Text = null;

            //Contato
            txtTelefone.Text = null;
            txtCelular.Text = null;
            txtOutros.Text = null;
            txtEmail.Text = null;

            //Curso
            ddlCurso.SelectedIndex = 0;

            //Programa Social
            ddlProgramaSocial.SelectedIndex = 0;
        }

        protected void btnLimpar_Click(object sender, EventArgs e)
        {
            limparCampos();
        }
    }
}