﻿using geducSite.Models;
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace geducSite.View
{
    public partial class alterar_nota : System.Web.UI.Page
    {

        protected IEnumerable<Nota> todasAsNotas;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["todasAsNotas"] != null)
            {
                todasAsNotas = (IEnumerable<Nota>)Session["todasAsNotas"];
            }
            else
            {
                todasAsNotas = new NotaDAO().Listar();
                Session["todasAsNotas"] = todasAsNotas;
                Session.Timeout = 6000;
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            Nota nota = todasAsNotas.SingleOrDefault(x=> x.idNota == Convert.ToInt32(txtid.Text));
            if (nota != null) {
                txtDate.Text = Convert.ToString(nota.data);
                txtNota.Text = Convert.ToString(nota.nota);
                txtOrigem.Text = nota.origem;
                txtPeriodo.Text = nota.periodo;
            }
        }

        protected void btnAlterarNota_Click(object sender, EventArgs e)
        {
            Nota nota = todasAsNotas.SingleOrDefault(x => x.idNota == Convert.ToInt32(txtid.Text));
            if (nota != null)
            {
                nota.data = Convert.ToDateTime(txtDate.Text);
                nota.nota = Convert.ToInt32(txtNota.Text);
                nota.origem = Convert.ToString(txtOrigem.Text);
                nota.periodo = Convert.ToString(txtPeriodo.Text);
                
                new NotaDAO().Alterar(nota);
            }
        }


    }
}