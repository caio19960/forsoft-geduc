﻿using geducSite.Context;
using geducSite.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace geducSite.Persistence
{
    public class CargoDAO : Conexao
    {
        public IEnumerable<Cargo> Listar()
        {
            try
            {
                AbrirConexao();
                Collection<Cargo> retorno = new Collection<Cargo>();

                cmd = new SqlCommand("select * from cargo", con);
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    var c = new Cargo();
                    c.idCargo = Convert.ToInt32(dr["idCargo"]);
                    c.cargo = Convert.ToString(dr["cargo"]);
                    c.funcao = Convert.ToString(dr["funcao"]);
                    retorno.Add(c);
                }
                return retorno;
            }
            finally
            {
                FecharConexao();

            }

        }
    }
}